/**
 * @author Hyejung Shin
 * 
 * This program is for weighted average
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class A5ScoreGui extends JFrame implements ActionListener {
	final static int SIZE = 4;
	static double wgAve;
	
	JButton jbtCalculate = new JButton("Calculate");
	JTextField [] jtxtScore = new JTextField[SIZE];
	JTextField [] jtxtWeight = new JTextField[SIZE];
	JLabel [] jlblModuleName = new JLabel[SIZE];
	JLabel result = new JLabel();
	JPanel scorePanel = new JPanel();
	JPanel buttonPanel = new JPanel();
	JPanel resultPanel = new JPanel();
	
	double [] scores = new double[SIZE];
	double [] weights = new double[SIZE];
	
	/**
	 * constructor
	 */
	public A5ScoreGui(){
		result.setText("--- RESULT ---");
		resultPanel.setBackground(Color.YELLOW);
		
		jlblModuleName[0] = new JLabel("Test 1");
		jlblModuleName[1] = new JLabel("Test 2");
		jlblModuleName[2] = new JLabel("Test 3");
		jlblModuleName[3] = new JLabel("Test 4");
		
		scorePanel.setBackground(Color.PINK);
		scorePanel.setLayout(new GridLayout(4,3));
		
		for (int i=0; i<4; i++){
			scorePanel.add(jlblModuleName[i]);
			jtxtScore[i] = new JTextField(8);
			scorePanel.add(jtxtScore[i]);
			jtxtWeight[i] = new JTextField(8);
			scorePanel.add(jtxtWeight[i]);
		}
		
		buttonPanel.setBackground(Color.PINK);
		buttonPanel.add(jbtCalculate);
		resultPanel.add(result);
		jbtCalculate.addActionListener(this);
		
		add(scorePanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.EAST);
		add(resultPanel, BorderLayout.SOUTH);
		
		setVisible(true);
		pack();
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * Calculate weighted average
	 * 
	 * @param scores
	 * @param weights
	 * @return
	 */
	public static double calculateAveScore (double [] scores, double [] weights){
		double [] calculate = new double [4];
		wgAve = 0.0;
		
		for (int i=0; i<SIZE; i++){
			calculate[i] = scores[i]*weights[i];
		}
		for (int j=0; j<calculate.length; j++){
			wgAve += calculate[j];
		}

		return wgAve;
	}
	
	/**
	 * print out the weighted average as result
	 * 
	 * @param grade
	 */
	public void actionPerformed(ActionEvent e){
		scores[0] = Double.parseDouble(jtxtScore[0].getText());
		scores[1] = Double.parseDouble(jtxtScore[1].getText());
		scores[2] = Double.parseDouble(jtxtScore[2].getText());
		scores[3] = Double.parseDouble(jtxtScore[3].getText());
		weights[0] = Double.parseDouble(jtxtWeight[0].getText());
		weights[1] = Double.parseDouble(jtxtWeight[1].getText());
		weights[2] = Double.parseDouble(jtxtWeight[2].getText());
		weights[3] = Double.parseDouble(jtxtWeight[3].getText());
		
		double grade = A5ScoreGui.calculateAveScore(scores, weights);
		
		result.setText(String.valueOf(wgAve));
	}
}