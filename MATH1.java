import java.util.Scanner;

public class MATH1 {
	public static void main(String [] args){
		Scanner keyboard = new Scanner(System.in);
		double H, F, G;
		double x, y, z;
		
		System.out.print("x: ");
		x = keyboard.nextDouble();
		System.out.print("y: ");
		y = keyboard.nextDouble();
		System.out.print("z: ");
		z = keyboard.nextDouble();
		
		H = Math.pow(x, 10);
		F = x+y;
		G = Math.sqrt(x) + Math.abs(y) + Math.pow(z, y);
		System.out.println("\n<RESULT>\n" + "h(x)= " + H + "\n" + "f(x,y)= " + F + "\n" + "g(x,y,z)= " + G);
	}
}