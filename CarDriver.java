import java.util.Scanner;

public class CarDriver {
	public static void main(String [] args){
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter a number to choose a car from 1 to 3: ");
		int Num = keyboard.nextInt();
		
		switch(Num){
		case 1:
			Car car1 = new Car("red", 350, 3);
			System.out.println("\n"+car1);
			break;
		case 2:
			Car car2 = new Car("yellow", 240, 3.5);
			System.out.println("\n"+car2);
			break;
		case 3:
			Car car3 = new Car("black", 150, 2);
			System.out.println("\n"+car3);
			break;
		}
	}
}
