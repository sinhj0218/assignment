import java.util.Scanner;

public class SwitchCase {
	public static void main(String [] args){
		Scanner keyboard = new Scanner(System.in);
		int choice;
		
		System.out.print(" 1. Administrator"
				+ "\n 2. Faculty"
				+ "\n 3. Staff"
				+ "\n 4. Student"
				+ "\n 5. Guest"
				+ "\nEnter a nubmer to check your role: ");
		choice = keyboard.nextInt();
		
		switch(choice){
			case 1:
				System.out.println("\nWelcome Administrator!");
				break;
			case 2:
				System.out.println("\nWelcome Faculty!");
				break;
			case 3:
				System.out.println("\nWelcome Staff!");
				break;
			case 4:
				System.out.println("\nWelcome Student!");
				break;
			default:
				System.out.println("\nWelcome Guest!");
		}
	}
}
