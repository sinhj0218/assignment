import java.util.Scanner;

public class RoomDriver {
	public static void main(String [] args){
		//TODO Auto-generated method stub
		//instantiation
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter the room number between 1 to 3: ");
		int Num = keyboard.nextInt();
		
		switch(Num){
		case 1:
			Room room1 = new Room("yellow", "hard-wood", 1);
			System.out.println("\n"+room1);
			break;
		case 2:
			Room room2 = new Room("purple", "tiled", 0);
			System.out.println("\n"+room2);
			break;
		case 3:
			Room room3 = new Room("white", "carpeted", 3);
			System.out.println("\n"+room3);
			break;
		default:
			System.out.println("\nWrong number! Choose a number between 1 to 3.");
		}
	}
}